var swiper;

$(function(){
    FastClick.attach(document.body);
    // gallery swiper
    swiper = new Swiper('.slider1', {
        slidesPerView: 1,

        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        on: {
            init: function () {
                checkVideos();
            },
        },
    });

    swiper.on('slideChange', function () {
        checkVideos();
    });

	onScrool();
});

function checkVideos() {
    $('.swiper-slide').each(function(){
        if ($(this).find('video').length) {
            $(this).find('video').get(0).pause();
        }
    });
    var currentVideo = $('.swiper-slide').eq(swiper ? swiper.activeIndex : 0).find('video');
    if (currentVideo.length) {
        currentVideo.get(0).play();
    }
}


//Sticky menu style change
function onScrool() {
    var top = $(document).scrollTop();
    if (top === 0) {
        $('.header').removeClass('onScrool');
    } else {
        $('.header').addClass('onScrool');
    }
}

$(window).on('scroll', onScrool);

$('.js-btnToggle').click(function (e) {
    e.preventDefault();
    $('.header').toggleClass('isActive');

})




$('.mainNav__link').click(function (e) {
    e.preventDefault();
    $('.header').removeClass('isActive');
    const id = $(this).data('href');


    $("html, body").animate({ scrollTop: $(`#${id}`).offset().top - $('.header').outerHeight()*0.9 }, 400);

})





$(window).resize(function () {
    setTimeout(function(){
        var instance = $('.sliderImg').data('vide');
        instance.resize();
    });
R


})

